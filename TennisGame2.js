var TennisGame2 = function(player1Name, player2Name) {
    this.P1point = 0;
    this.P2point = 0;

    this.P1res = "";
    this.P2res = "";

    this.player1Name = player1Name;
    this.player2Name = player2Name;
};

function isDeuce(player1Point, player2Point) {
    if (player1Point === player2Point && player1Point > 2) {
        return true;
    } else {
        return false;
    }
}

function winFifteen(point) {
    if (point === 1)
        return true;
    else return false;
}

function winThirty(point) {
    if (point === 2)
        return true;
    else return false;
}

function winForty(point) {
    if (point === 3)
        return true;
    else return false;
}

function whoHasAdvantage(p1point, p2point) {
    if (p1point > p2point && p2point >= 3) {
        return "Advantage player1";
    }
    if (p2point > p1point && p1point >= 3) {
        return "Advantage player2";
    }
    return '';
}

function whoIsTheWinner(p1point, p2point, score) {
    if (p1point >= 4 && p2point >= 0 && (p1point - p2point) >= 2) {
        return "Win for player1";
    }
    if (p2point >= 4 && p1point >= 0 && (p2point - p1point) >= 2) {
        return "Win for player2";
    }
    return score;
}

TennisGame2.prototype.getScore = function() {
    var score = "";

    if (this.P1point === this.P2point && this.P1point < 3) {
        if (this.P1point === 0)
            score = "Love";
        if (this.P1point === 1)
            score = "Fifteen";
        if (this.P1point === 2)
            score = "Thirty";
        score += "-All";
    }

    if (isDeuce(this.P1point, this.P2point))
        score = "Deuce";


    if (this.P1point > 0 && this.P2point === 0) {
        let res;
        if (winFifteen(this.P1point)) {
            this.P1res = "Fifteen";
        };
        if (winThirty(this.P1point)) {
            this.P1res = "Thirty";
        };
        if (winForty(this.P1point)) {
            this.P1res = "Forty";
        };
        this.P2res = "Love";
        score = this.P1res + "-" + this.P2res;
    }

    if (this.P2point > 0 && this.P1point === 0) {
        if (winFifteen(this.P2point)) {
            this.P2res = "Fifteen";
        };
        if (winThirty(this.P2point)) {
            this.P2res = "Thirty";
        };
        if (winForty(this.P2point)) {
            this.P2res = "Forty";
        };
        this.P1res = "Love";
        score = this.P1res + "-" + this.P2res;
    }

    if (this.P1point > this.P2point && this.P1point < 4) {
        if (this.P1point === 2)
            this.P1res = "Thirty";
        if (this.P1point === 3)
            this.P1res = "Forty";
        if (this.P2point === 1)
            this.P2res = "Fifteen";
        if (this.P2point === 2)
            this.P2res = "Thirty";
        score = this.P1res + "-" + this.P2res;
    }
    if (this.P2point > this.P1point && this.P2point < 4) {
        if (this.P2point === 2)
            this.P2res = "Thirty";
        if (this.P2point === 3)
            this.P2res = "Forty";
        if (this.P1point === 1)
            this.P1res = "Fifteen";
        if (this.P1point === 2)
            this.P1res = "Thirty";
        score = this.P1res + "-" + this.P2res;
    }

    score += whoHasAdvantage(this.P1point, this.P2point);
    score = whoIsTheWinner(this.P1point, this.P2point, score);
    return score;
};

TennisGame2.prototype.SetP1Score = function(number) {
    var i;
    for (i = 0; i < number; i++) {
        this.P1Score();
    }
};

TennisGame2.prototype.SetP2Score = function(number) {
    var i;
    for (i = 0; i < number; i++) {
        this.P2Score();
    }
};

TennisGame2.prototype.P1Score = function() {
    this.P1point++;
};

TennisGame2.prototype.P2Score = function() {
    this.P2point++;
};

TennisGame2.prototype.wonPoint = function(player) {
    if (player === "player1")
        this.P1Score();
    else
        this.P2Score();
};

if (typeof window === "undefined") {
    module.exports = TennisGame2;
}